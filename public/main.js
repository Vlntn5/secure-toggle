const callbackurl = $("#callback-url").data("url");
const interface = $("#interface");
const imghelper = $('#img-helper');

var securityCheck = {
    sde: false,
    xss: false,
    csrf: false
}

$(".spoiler-trigger").click(function() {
    $('.spoiler-trigger').each(function(i, obj) {
        $(this).removeClass("btn-info");

        if( ! $(this).hasClass("btn-secondary")) {
            $(this).addClass("btn-secondary")
        }
        
        $(this).parent().next().collapse('hide');
    });

    $(this).removeClass("btn-secondary");
    $(this).addClass("btn-info");
    $(this).parent().next().collapse('toggle');
});

function sendSde() {
    var data = $("#secure-sde-data").val();

    $.post(callbackurl + "sde/store", {
        safe: securityCheck["sde"],
        data: data
    },
    function(data, status) {
        logOnInterface(data);
        $("#secure-sde-data").val("");
    })
}

function attackSde() {
    $.post(callbackurl + "sde/attack",
    function(data, status) {
        logOnInterface(data);
    })
}

function attackXSSStored() {
    $.post(callbackurl + "xss/stored", {safe: securityCheck['xss']},
    function(data, status) {
        logOnInterface("Received image source stored by corrupt user");
        logOnInterface("Loading it in ... ");

        imghelper.html(`<img src=${data.data}>`);
        if(data.safe == 'true') {
            logOnInterface("Nothings happens because of filter, sanitize<br/> and putting it in quotation marks")
        }
    })
}

function storeCsrf() {
    var data = $('#secure-csrf-data').val();
    var token = $('#token').data('token');

    $.post(callbackurl + "csrf/store", {
        funds: data,
        token: token
    },
    function(data, status){
        logOnInterface(data);
    })
}

function getCsrf() {
    $.post(callbackurl + 'csrf/state',
    function(data, status){
        logOnInterface(data);
    })
}

function getNowDateTime() {
    var datetime = new Date($.now());
    return "[" + datetime.toISOString().replace("T", " ")
    .replace("Z", "] ");
}

function attackCsrf() {
    logOnInterface("Adding an img with corrupted src..");
    
    imghelper.html(`<img src=${callbackurl + 'csrf/attack?transfer=thief'}>`);

    logOnInterface("Attack has passed, check state of funds now");
}

function logOnInterface(data) {
    data = data.replace(/\+/g, " ");

    if(interface.data("empty")) {
        interface.html(getNowDateTime() + data);
        interface.data("empty", false);
    } else {
        interface.html(interface.html() + "<br/>" + getNowDateTime() + data);
    }
}

$(function() {
    $("#sde-toggle-btn").click();

    $("#toggle-one").change(function() {
        securityCheck["sde"] = $(this).prop('checked');
    })
    
    $("#toggle-two").change(function() {
        securityCheck["xss"] = $(this).prop('checked');
        $(".xss-toggler").each(function(i, obj) {
            if(securityCheck["xss"])
                $(this).attr('method', 'POST')
            else
                $(this).attr('method', 'GET')
        });
    })

    $("#toggle-three").change(function() {
        $.post(callbackurl + 'csrf/setsafe', {
            safe: $(this).prop('checked')
        })

        securityCheck["csrf"] = $(this).prop('checked');
    })
})