const port = process.env.PORT || 4080;
const https = require('https');
const cookie = require('cookie');
const fs = require('fs');
const uuid = require('uuid');

const dotenv = require('dotenv');
dotenv.config();

const express = require('express');
const app = express();

const sdeRouter = require('./routes/sdeRoute');
const xssRouter = require('./routes/xssRoute');
const csrfRouter = require('./routes/csrfRoute');

app.use(express.static('public'));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

//validate tokens and cookies
app.use(function(req, res, next) {
    var cookieId = cookie.parse(req.headers.cookie).id;
    var token = req.query.token || req.body.token;

    if(token && validTokensForCookies[cookieId] && token == validTokensForCookies[cookieId]) {
        res.locals.tokenVerified = true;
    } else {
        res.locals.tokenVerified = false;
    }

    console.log("Token verified for " + token + " and " + validTokensForCookies[cookieId] + "\n -->" + res.locals.tokenVerified)
        
    next();
});

app.use('/sde', sdeRouter);
app.use('/xss', xssRouter);
app.use('/csrf', csrfRouter);

app.set('view engine', 'ejs');

var validTokensForCookies = {}

app.get('/', function (req, res) {

    //httponly set to false for XSS purpouse, in other cases true is a must
    if (!req.headers.cookie) {
        var cookieId = uuid.v4();

        res.setHeader('Set-Cookie', cookie.serialize('id', cookieId, {
            httpOnly: false,
            maxAge: 60 * 60
        }));
    
        res.statusCode = 302;
        res.setHeader('Location', req.headers.referer || '/');
        res.end();
        return;
    }

    var cookieId = cookie.parse(req.headers.cookie).id;
    
    if(!validTokensForCookies[cookieId])
        validTokensForCookies[cookieId]= uuid.v4();

    res.render('index', {
        urlcallback: process.env.LOCATION_STORAGE || "https://localhost:4080/",
        userinput: req.query.usersend,
        msg: req.query.additionalmessage,
        token: validTokensForCookies[cookieId]
    }); 
})

if (process.env.PORT) {
    app.listen(port, function () {
        console.log(`Server running at https://localhost:${port}/`);
    });
} else {
    https.createServer({
            key: fs.readFileSync('./certificates/server.key'),
            cert: fs.readFileSync('./certificates/server.cert')
        }, app)
        .listen(port, function () {
            console.log(`Server running at https://localhost:${port}/`);
        });
}