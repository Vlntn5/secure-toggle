const express = require('express');
const url = require('url');
const sanitize = require('sanitize-html');
const { EWOULDBLOCK } = require('constants');
const router = express.Router();

const storedImgLink = 'x" onerror=javascript:logOnInterface(document.cookie);';

//vulnerable method
router.get('/reflect', function(req, res) {
    res.redirect(url.format({
        pathname: '/',
        query: {
            usersend: req.query.usersend
        }
    }))
})

//safe method
router.post('/reflect', function(req, res){

    //sanitize
    var param = sanitize(req.body.usersend);

    //filter
    param = param.replace(/</g, "").replace(/>/g, "")
        .replace(/{/g, "").replace(/}/g, "")
        .replace(/"/g, "").replace(/'/g, "")
        .replace(/javascript:/g, "")
    
    param = '"' + param + '"'

    res.redirect(url.format({
        pathname: '/',
        query: {
            usersend: param,
            additionalmessage: "Prevented reflect XSS from happening with filtering"
        }
    }))
})


router.post('/stored', function(req, res) {

    //assume because of worst case there is corrupt link already stored
    var param = storedImgLink;

    if(req.body.safe == 'true') {
        
        param = sanitize(param);

        param = param.replace(/</g, "").replace(/>/g, "")
        .replace(/{/g, "").replace(/}/g, "")
        .replace(/"/g, "").replace(/'/g, "")
        .replace(/javascript:/g, "")

        param = '"' + param + '"'
    }

    res.status(200).send(
        {data: param, safe: req.body.safe}
    )
})


router.get('/dom', function(req, res) {
    res.redirect(url.format({
        pathname: '/',
        query: {
            domsend: req.query.domsend
        }
    }))
})

router.post('/dom', function(req, res) {
    //sanitize
    var param = sanitize(req.body.domsend);

    //filter
    param = param.replace(/</g, "").replace(/>/g, "")
        .replace(/{/g, "").replace(/}/g, "")
        .replace(/"/g, "").replace(/'/g, "")
        .replace(/javascript:/g, "")
    
    param = '"' + param + '"'

    res.redirect(url.format({
        pathname: '/',
        query: {
            domsend: param,
            additionalmessage: "DOM XSS failed because of usual safety methods"
        }
    }))
})


module.exports = router;