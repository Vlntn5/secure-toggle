const express = require('express');
const url = require('url');
const cookies = require('cookie');
const router = express.Router();

var funds = {};
var safe = 'false';

router.post('/setsafe', function(req, res) {
    safe = req.body.safe;
    res.status(200).send("Safe state changed to " + req.body.safe)
})


router.post('/store', function(req, res) {
    var id = cookies.parse(req.headers.cookie).id;

    if(safe == 'true') {
        if(!res.locals.tokenVerified) {
            res.redirect(url.format({
                pathname: '/',
                query: {
                    additionalmessage: "[SERVER]: Store failed, token not valid :)"
                }
            }))
            return;
        }
    }

    funds[id] = req.body.funds;

    res.status(200).send(`Set funds to ${req.body.funds} for ${id}`);
})

router.post('/state', function(req, res) {
    res.status(200).send(JSON.stringify(funds));
})

router.get('/attack', function(req, res) {
    var id = cookies.parse(req.headers.cookie).id;

    if(safe == 'true') {
        if(!res.locals.tokenVerified) {
            res.end();
            return;
        }
    }

    funds[req.query.transfer] = funds[id];
    funds[id] = 0;

    res.end()
});

module.exports = router;