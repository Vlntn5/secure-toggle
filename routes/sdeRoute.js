const express = require('express');
const crypto = require('crypto');
const router = express.Router();

var dataStorage = {
    safeSaved: [],
    badSaved: []
};

router.post('/store', function(req, res) {
    var {safe, data} = req.body;

    if (safe == 'true') {
        dataStorage['safeSaved'].push(crypto.createHash('sha256').update(data).digest('hex'));
    } else {
        dataStorage['badSaved'].push(data);
    }

    console.log(`Stored (${safe}) :: ${data}`);

    res.status(201).send(`Data stored to server (safe = ${safe})`);
})

router.post('/attack', function(req, res) {
    var stringToSend = "<br/>Data leak from the attack: <br/><br/> Secured data :: <br/>"

    dataStorage.safeSaved.forEach((value) => {
        stringToSend += value + "<br/>"
    })
    
    stringToSend += "<br/>Non secured data :: <br/>"
    
    dataStorage.badSaved.forEach((value) => {
        stringToSend += value + "<br/>"
    })

    res.send(stringToSend);
})


module.exports = router;